package com.example.task04;

import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Locale;

public class Task04Main {
    public static void main(String[] args) throws IOException {
        // чтобы протестировать свое решение, вам нужно:
        // - направить файл input.test в стандартный ввод программы (в настройках запуска программы в IDE или в консоли)
        // - запустить программу
        // - проверить, что получилось 351.731900

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String line;
        double sum = 0.0;

        while ((line = reader.readLine()) != null) {
            String[] words = line.split("\\s+");
            for (String word : words) {
                try {
                    double number = Double.parseDouble(word);
                    sum += number;
                } catch (NumberFormatException e) {
                }
            }
        }

        Locale.setDefault(Locale.ENGLISH);
        System.out.printf("%.6f%n", sum);

    }
}
