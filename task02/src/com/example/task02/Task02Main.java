package com.example.task02;

import java.io.IOException;

public class Task02Main {
    public static void main(String[] args) throws IOException {
        // чтобы протестировать свое решение, вам нужно:
        // - направить файл input.test в стандартный ввод программы (в настройках запуска программы в IDE или в консоли)
        // - направить стандартный вывод программы в файл output.test
        // - запустить программу
        // - и сравнить получившийся файл output.test с expected.test

        int prev_byte_of_stream = 0;

        for (int byte_of_stream; (byte_of_stream = System.in.read()) != -1; prev_byte_of_stream = byte_of_stream) {
            if (byte_of_stream == '\n') {
                System.out.write('\n');
            } else {
                if (prev_byte_of_stream == '\r') {
                    System.out.write(prev_byte_of_stream);
                }

                if (byte_of_stream != '\r') {
                    System.out.write(byte_of_stream);
                }
            }
        }
        if (prev_byte_of_stream == '\r') {
            System.out.write(prev_byte_of_stream);
        }
    }


}
